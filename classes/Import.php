<?php

require_once 'Match.php';

/**
 * Base Import class that can be extended for individual file/data types
 */
abstract class Import
{
    /**
     * @var \PDO
     */
    protected $dbh;

    /**
     * @var string
     */
    protected $rawData;

    /**
     * @param array $parsedData
     */
    protected $parsedData;

    /**
     * @param \PDO $dbh
     * @param string $rawData
     */
    public function __construct(\PDO $dbh, $rawData)
    {
        $this->dbh        = $dbh;
        $this->rawData    = $rawData;
        $this->parsedData = [];
    }

    /**
     * Check if the raw data is valid for the given import type
     *
     * @return bool
     */
    abstract public function isValidInput();

    /**
     * Import match results based on parsed data
     */
    public function import()
    {
        $matchDate = $mapId = $team1Id = $team2Id = $score1 = $score2 = null;

        // basic insert statement based on parsed data; this will update the map and scores if
        // a duplicate is found, duplicate based on unique row of (team1_id, team2_id, match_date)
        $stmt = $this->dbh->prepare('
            INSERT INTO match_result
                (match_date, map_id, team1_id, team2_id, score1, score2, created_at)
            VALUES
                (:matchDate, :mapId, :team1Id, :team2Id, :score1, :score2, NOW())
            ON DUPLICATE KEY UPDATE
                map_id = VALUES(map_id), score1 = VALUES(score1), score2 = VALUES(score2), updated_at = NOW()
        ');

        $stmt->bindParam(':matchDate', $matchDate, \PDO::PARAM_STR);
        $stmt->bindParam(':mapId', $mapId, \PDO::PARAM_INT);
        $stmt->bindParam(':team1Id', $team1Id, \PDO::PARAM_INT);
        $stmt->bindParam(':team2Id', $team2Id, \PDO::PARAM_INT);
        $stmt->bindParam(':score1', $score1, \PDO::PARAM_INT);
        $stmt->bindParam(':score2', $score2, \PDO::PARAM_INT);

        // loop through each row of parsed data and check that the match is valid; while checking for
        // the valid match, the appropriate strings for map/teams are converted to their database ids
        foreach ($this->parsedData as $row) {
            $match = $this->checkValidMatch($row);

            $matchDate = $match->date;
            $mapId     = $match->map;
            $team1Id   = $match->team1;
            $team2Id   = $match->team2;
            $score1    = $match->score1;
            $score2    = $match->score2;

            $stmt->execute();
        }

        // the way duplicates were updated above, update all wins/losses/ties for each team
        $this->updateTeamTotals();
    }

    /**
     * Check if a match is valid and set the values in a way that the database can insert
     *
     * @param \stdClass $row
     * @return \Match
     * @throws \Exception
     */
    private function checkValidMatch(\stdClass $row)
    {
        // first make sure all fields exist
        if (
            !property_exists($row, 'date') ||
            !property_exists($row, 'map') ||
            !property_exists($row, 'team1') ||
            !property_exists($row, 'team2') ||
            !property_exists($row, 'score1') ||
            !property_exists($row, 'score2')
        ) {
            throw new \Exception('A match did not contain valid data');
        }

        // set the match class and appropriate database values
        $match = new Match($this->dbh);
        $match->checkAndSetValues($row);

        return $match;
    }

    /**
     * Update all team totals for wins/losses/ties; could have done this while looping and inserting matches, but
     * with the ON DUPLICATE KEY UPDATE, went this route instead...the query itself runs quickly enough for 1000 matches
     */
    private function updateTeamTotals()
    {
        $stmt = $this->dbh->prepare('
            UPDATE team t
                INNER JOIN (
                  SELECT   team1_id, COUNT(score1) AS team1_wins_total
                  FROM     match_result
                  WHERE    score1 = 16
                  GROUP BY team1_id
                ) mrw1 ON t.id = mrw1.team1_id
                INNER JOIN (
                  SELECT   team2_id, COUNT(score2) AS team2_wins_total
                  FROM     match_result
                  WHERE    score2 = 16
                  GROUP BY team2_id
                ) mrw2 ON t.id = mrw2.team2_id
                INNER JOIN (
                  SELECT   team1_id, COUNT(score1) AS team1_losses_total
                  FROM     match_result
                  WHERE    score1 < 15
                  GROUP BY team1_id
                ) mrl1 ON t.id = mrl1.team1_id
                INNER JOIN (
                  SELECT   team2_id, COUNT(score2) AS team2_losses_total
                  FROM     match_result
                  WHERE    score2 < 15
                  GROUP BY team2_id
                ) mrl2 ON t.id = mrl2.team2_id
                INNER JOIN (
                  SELECT   team1_id, COUNT(score1) AS team1_ties_total
                  FROM     match_result
                  WHERE    score1 = 15
                  GROUP BY team1_id
                ) mrt1     ON t.id = mrt1.team1_id
                INNER JOIN (
                  SELECT   team2_id, COUNT(score2) AS team2_ties_total
                  FROM     match_result
                  WHERE    score2 = 15
                  GROUP BY team2_id
                ) mrt2 ON t.id = mrt2.team2_id
            SET
                t.wins   = mrw1.team1_wins_total + mrw2.team2_wins_total,
                t.losses = mrl1.team1_losses_total + mrl2.team2_losses_total,
                t.ties   = mrt1.team1_ties_total + mrt2.team2_ties_total
        ');

        $stmt->execute();
    }
}