<?php

require_once 'Import.php';

/**
 * Import class for CSV data
 */
class ImportJson extends Import
{
    /**
     * {@inheritdoc}
     */
    public function isValidInput()
    {
        // try to decode the raw data into parsed data, and check if there was an error
        $this->parsedData = json_decode($this->rawData);

        return (json_last_error() === JSON_ERROR_NONE);
    }
}