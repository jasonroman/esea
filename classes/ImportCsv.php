<?php

require_once 'Import.php';

/**
 * Import class for CSV data
 */
class ImportCsv extends Import
{
    /**
     * Maps each match data key to its corresponding index in the parsed CSV row
     *
     * @var array
     */
    private static $ROW_TO_OBJECT = [
        'date'   => 0,
        'map'    => 1,
        'team1'  => 2,
        'team2'  => 3,
        'score1' => 4,
        'score2' => 5,
    ];

    /**
     * {@inheritdoc}
     */
    public function isValidInput()
    {
        // first read all data and split by each row
        $csvData = str_getcsv($this->rawData, "\n");

        // update each row as a \stdClass with corresponding keys for a match
        foreach ($csvData as &$row) {
            $row = str_getcsv($row);

            // should have six values per row
            if (count($row) !== 6) {
                return false;
            }

            // map the row to a match object
            $match = new \stdClass();

            foreach (self::$ROW_TO_OBJECT as $key => $index) {
                $match->$key = $row[$index];
            }

            $this->parsedData[] = (object) $match;
        }

        return true;
    }
}