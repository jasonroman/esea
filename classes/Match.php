<?php

require 'lib/Utility.php';

/**
 * Match class that handles checking for valid data and converting parsed data to database values for inserting
 */
class Match
{
    /**
     * @var \PDO
     */
    private $dbh;

    /**
     * @var string
     */
    public $date;

    /**
     * @var int
     */
    public $map;

    /**
     * @var int
     */
    public $team1;

    /**
     * @var int
     */
    public $team2;

    /**
     * @var int
     */
    public $score1;

    /**
     * @var int
     */
    public $score2;

    /**
     * @param \PDO $dbh
     */
    public function __construct(\PDO $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * Check that all match values are valid, and convert to corresponding database ids where appropriate
     *
     * @param \stdClass $match
     */
    public function checkAndSetValues(\stdClass $match)
    {
        $this->checkDate($match->date);
        $this->checkMap($match->map);
        $this->checkTeams($match->team1, $match->team2);
        $this->checkTeam1($match->team1);
        $this->checkTeam2($match->team2);
        $this->checkScore1($match->score1);
        $this->checkScore2($match->score2);
        $this->checkScores($this->score1, $this->score2);
    }

    /**
     * Checks that the date is valid and sets it as UTC
     *
     * @param string $date
     */
    private function checkDate($date)
    {
        $this->date = new \DateTime($date, new \DateTimeZone('UTC'));
        $this->date = $this->date->format('Y-m-d g:i:s e');
    }

    /**
     * Checks that the map has a corresponding database id and sets it
     *
     * @param string $map
     */
    private function checkMap($map)
    {
        $this->map = getMapIdByName($this->dbh, $map);
    }

    /**
     * Makes sure that a team is not playing against itself
     *
     * @param string $team1
     * @param string $team2
     * @throws \Exception
     */
    private function checkTeams($team1, $team2)
    {
        if ($team1 === $team2) {
            throw new \Exception('A team cannot play itself in a match');
        }
    }

    /**
     * Checks that team 1 has a corresponding database id and sets it
     *
     * @param string $team
     */
    private function checkTeam1($team)
    {
        $this->team1 = $this->checkTeam($team);
    }

    /**
     * Checks that team 2 has a corresponding database id and sets it
     *
     * @param string $team
     */
    private function checkTeam2($team)
    {
        $this->team2 = $this->checkTeam($team);
    }

    /**
     * Checks that the given team has a corresponding database id and sets it
     *
     * @param string $team
     */
    private function checkTeam($team)
    {
        return getTeamIdByName($this->dbh, $team);
    }

    /**
     * Checks that the score of the match is valid; either 15/15 or one team with 16, and no more than 30 total rounds
     *
     * @param string $team
     */
    private function checkScores($score1, $score2)
    {
        if ($score1 === 15) {
            if ($score2 !== 15) {
                throw new \Exception('Ties must have both teams winning 15 rounds');
            } else {
                return;
            }
        }

        if (!($score1 === 16 || $score2 === 16)) {
            throw new \Exception('Neither team won the match');
        } elseif ($score1 + $score2 > 30) {
            throw new \Exception('There cannot be more than 30 rounds played in the match');
        }
    }

    /**
     * Checks that score 1 is between 0 and 16 and sets it as an int
     *
     * @param string|int $score
     */
    private function checkScore1($score)
    {
        $this->score1 = $this->checkScore($score);
    }

    /**
     * Checks that score 2 is between 0 and 16 and sets it as an int
     *
     * @param string|int $score
     */
    private function checkScore2($score)
    {
        $this->score2 = $this->checkScore($score);
    }

    /**
     * Checks that the given score is between 0 and 16 and converts it to an integer
     *
     * @param type $score
     */
    private function checkScore($score)
    {
        if (!is_numeric($score) || $score < 0 || $score > 16) {
            throw new \Exception('Score must be between 0 and 16');
        }

        return (int) $score;
    }
}