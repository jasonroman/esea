<?php

// delete all match results and team win/loss/tie records so various data can be tested and re-tested

require_once 'lib/Database.php';

$dbh->query('DELETE FROM match_result');
$dbh->query('UPDATE team SET wins = 0, losses = 0, ties = 0, updated_at = NULL');