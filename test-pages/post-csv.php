<?php

// import the test CSV file
$url  = 'http://esea.jayroman.com/index.php';
$data = ['data' => file_get_contents('data.csv')];

$options = [
    'http' => [
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    ]
];

$context = stream_context_create($options);

var_dump(file_get_contents($url, false, $context));