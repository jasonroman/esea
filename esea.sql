-- phpMyAdmin SQL Dump
-- version 4.2.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 28, 2016 at 09:20 PM
-- Server version: 5.5.44-0ubuntu0.14.10.1
-- PHP Version: 5.5.12-2ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `esea`
--

-- --------------------------------------------------------

--
-- Table structure for table `map`
--

CREATE TABLE IF NOT EXISTS `map` (
`id` smallint(5) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `map`
--

INSERT INTO `map` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'de_cache', '2016-05-28 17:13:02', NULL),
(2, 'de_cbble', '2016-05-28 17:13:02', NULL),
(3, 'de_dust2', '2016-05-28 17:13:02', NULL),
(4, 'de_inferno', '2016-05-28 17:13:02', NULL),
(5, 'de_mirage', '2016-05-28 17:13:02', NULL),
(6, 'de_nuke', '2016-05-28 17:13:02', NULL),
(7, 'de_overpass', '2016-05-28 17:13:02', NULL),
(8, 'de_train', '2016-05-28 17:13:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `match_result`
--

CREATE TABLE IF NOT EXISTS `match_result` (
`id` int(10) unsigned NOT NULL,
  `map_id` smallint(5) unsigned NOT NULL,
  `team1_id` int(10) unsigned NOT NULL,
  `team2_id` int(10) unsigned NOT NULL,
  `score1` tinyint(3) unsigned NOT NULL,
  `score2` tinyint(3) unsigned NOT NULL,
  `match_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wins` int(10) unsigned NOT NULL DEFAULT '0',
  `losses` int(10) unsigned NOT NULL DEFAULT '0',
  `ties` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `wins`, `losses`, `ties`, `created_at`, `updated_at`) VALUES
(1, 'astralis', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(2, 'Cloud9', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(3, 'compLexity', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(4, 'Counter Logic Gaming', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(5, 'FaZe', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(6, 'FlipSid3 Tactics', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(7, 'fnatic', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(8, 'G2 Kinguin', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(9, 'KKona', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(10, 'Luminosity Gaming', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(11, 'mousesports', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(12, 'Natus Vincere', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(13, 'Ninjas in Pyjamas', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(14, 'NRG eSports', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(15, 'OpTic Gaming', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(16, 'Renegades', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(17, 'Selfless Gaming', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(18, 'SK Gaming', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(19, 'Splyce', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(20, 'Team Dignitas', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(21, 'TEAM ENVYUS', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(22, 'Team Liquid', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(23, 'Virtus  Pro', 0, 0, 0, '2016-05-28 19:03:41', NULL),
(24, 'Winterfox', 0, 0, 0, '2016-05-28 19:03:41', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `map`
--
ALTER TABLE `map`
 ADD PRIMARY KEY (`id`),
 ADD UNIQUE KEY `name` (`name`)
;

--
-- Indexes for table `match_result`
--
ALTER TABLE `match_result`
 ADD PRIMARY KEY (`id`),
 ADD UNIQUE KEY `uq_match` (`team1_id`, `team2_id`, `match_date`),
 ADD KEY `map_id` (`map_id`),
 ADD KEY `team1_id` (`team1_id`),
 ADD KEY `team2_id` (`team2_id`)
;

--
-- Indexes for table `team`
--
ALTER TABLE `team`
 ADD PRIMARY KEY (`id`),
 ADD UNIQUE KEY `name` (`name`),
 ADD KEY `wins` (`wins`, `losses`, `ties`)
;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `map`
--
ALTER TABLE `map`
MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `match_result`
--
ALTER TABLE `match_result`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `match_result`
--
ALTER TABLE `match_result`
ADD CONSTRAINT `fk_match_map` FOREIGN KEY (`map_id`) REFERENCES `map` (`id`),
ADD CONSTRAINT `fk_match_team1` FOREIGN KEY (`team1_id`) REFERENCES `team` (`id`),
ADD CONSTRAINT `fk_match_team2` FOREIGN KEY (`team2_id`) REFERENCES `team` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
