<?php

// try connecting to the database; could hide credentials in another file as future enhancement
$dsn      = 'mysql:dbname=esea;host=127.0.0.1';
$username = 'esea';
$password = 'e3s5e3a4';

try {
    $dbh = new \PDO($dsn, $username, $password);
    $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
} catch (\Exception $e) {
    http_response_code(500);
    exit();
}