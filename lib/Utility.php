<?php

/**
 * Retrieve a team id by team name
 *
 * @param \PDO $dbh
 * @param string $team
 * @throws \Exception
 */
function getTeamIdByName(\PDO $dbh, $team)
{
    $stmt = $dbh->prepare('SELECT id FROM team WHERE name = :name');
    $stmt->execute(['name' => $team]);

    if (!($teamId = $stmt->fetchColumn())) {
        throw new \Exception('Could not find team in database');
    }

    return $teamId;
}

/**
 * Retrieve a map id by map name
 *
 * @param \PDO $dbh
 * @param string $map
 * @throws \Exception
 */
function getMapIdByName(\PDO $dbh, $map)
{
    $stmt = $dbh->prepare('SELECT id FROM map WHERE name = :name');
    $stmt->execute(['name' => $map]);

    if (!($mapId = $stmt->fetchColumn())) {
        throw new \Exception('Could not find map in database');
    }

    return $mapId;
}