<?php

/**
 * Handle a GET request to output top ten teams by record or a team's record in a specific date range
 *
 * @param \PDO $dbh
 * @throws \Exception
 */
function handleGetRequest(\PDO $dbh)
{
    if (!empty($_GET['team']) && !empty($_GET['date_start']) && !empty($_GET['date_end'])) {
        $output = getTeamRecordInDateRange($dbh);
    } else {
        $output = getTopTenTeamsByRecord($dbh);
    }

    http_response_code(200);
    header('Content-Type: application/json');
    echo json_encode($output);
}

/**
 * Return the top ten teams by their individual record
 *
 * @param \PDO $dbh
 * @return array
 */
function getTopTenTeamsByRecord(\PDO $dbh)
{
    $stmt = $dbh->prepare('
        SELECT   name AS team, wins, losses, ties
        FROM     team t
        ORDER BY wins DESC, losses ASC, ties ASC
        LIMIT 10
    ');
    $stmt->execute();

    return $stmt->fetchAll(\PDO::FETCH_ASSOC);
}

/**
 * Return an individual team's record within a specific date range
 *
 * @param \PDO $dbh
 * @return type
 */
function getTeamRecordInDateRange(\PDO $dbh)
{
    // sanitize the GET parameters and convert the dates to a \DateTime object with UTC timezone
    $team      = filter_input(INPUT_GET, 'team', FILTER_SANITIZE_STRING);
    $startDate = new \DateTime(filter_input(INPUT_GET, 'date_start', FILTER_SANITIZE_STRING), new \DateTimeZone('UTC'));
    $endDate   = new \DateTime(filter_input(INPUT_GET, 'date_end', FILTER_SANITIZE_STRING), new \DateTimeZone('UTC'));

    // looks spookier than it is; simply takes a team and gets its total number of wins, losses, and ties
    // both as team1 and team2 in the match results, and also restricts by the range between start and end date
    $stmt = $dbh->prepare("
        SELECT
            name,
            COALESCE(SUM(wins), 0) AS wins,
            COALESCE(SUM(losses), 0) AS losses,
            COALESCE(SUM(ties), 0) AS ties
        FROM (
            SELECT
                t1.name,
                SUM(CASE WHEN score1 = 16 THEN 1 ELSE 0 END) AS wins,
                SUM(CASE WHEN score1 < 15 THEN 1 ELSE 0 END) AS losses,
                SUM(CASE WHEN score1 = 15 THEN 1 ELSE 0 END) AS ties
            FROM       match_result mr1
            INNER JOIN team t1 ON mr1.team1_id = t1.id
            WHERE
                t1.name = :team
            AND mr1.match_date BETWEEN :startDate AND :endDate
        UNION
            SELECT
                t2.name,
                SUM(CASE WHEN score2 = 16 THEN 1 ELSE 0 END) AS wins,
                SUM(CASE WHEN score2 < 15 THEN 1 ELSE 0 END) AS losses,
                SUM(CASE WHEN score2 = 15 THEN 1 ELSE 0 END) AS ties
            FROM       match_result mr2
            INNER JOIN team t2 ON mr2.team2_id = t2.id
            WHERE
                t2.name = :team
            AND mr2.match_date BETWEEN :startDate AND :endDate
        ) x
    ");

    $stmt->execute([
        'team'      => $team,
        'startDate' => $startDate->format('Y-m-d g:i:s e'),
        'endDate'   => $endDate->format('Y-m-d g:i:s e'),
    ]);

    return $stmt->fetchAll(\PDO::FETCH_ASSOC);
}