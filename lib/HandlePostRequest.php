<?php

/**
 * Handle a POST request with CSV or JSON submitted data
 *
 * @param \PDO $dbh
 * @throws \Exception
 */
function handlePostRequest(\PDO $dbh)
{
    try {
        // expect the input values in the 'data' parameter of the POST request
        if (!($data = filter_input(INPUT_POST, 'data', FILTER_SANITIZE_STRING))) {
            throw new \Exception('Invalid input data');
        }

        $dbh->beginTransaction();

        // first check if JSON input, and if so handle
        $importJson = new ImportJson($dbh, $data);

        if ($importJson->isValidInput()) {
            $importJson->import();

        // otherwise this should be a CSV
        } else {
            $importCsv = new ImportCsv($dbh, $data);

            if ($importCsv->isValidInput()) {
                $importCsv->import();
            } else {
                throw new \Exception('Input data must be in valid JSON or CSV format');
            }
        }

        $dbh->commit();

    } catch (\Exception $e) {
        $dbh->rollBack();

        http_response_code(500);
        exit();
    }

    http_response_code(200);
}