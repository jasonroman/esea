<?php

require_once 'classes/ImportCsv.php';
require_once 'classes/ImportJson.php';

// make sure that the client sent a GET or POST request
$requestMethod = filter_input(INPUT_SERVER, 'REQUEST_METHOD');

if (!in_array($requestMethod, ['GET', 'POST'])) {
    exit(header('Status: 500'));
}

require_once 'lib/Database.php';
require_once 'lib/HandleGetRequest.php';
require_once 'lib/HandlePostRequest.php';

// handle the request based on the request method
($requestMethod === 'POST') ? handlePostRequest($dbh) : handleGetRequest($dbh);